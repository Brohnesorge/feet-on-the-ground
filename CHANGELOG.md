## Version 0.0.8b - October 14, 2024
# + GAMEPLAY +
## General
* Fixed crash when Scarcity options are used
* Improved Scarcity code
* Improved casing spawning
* Explosives now leave decals
* Bullets and explosives now leave marks on floors and ceilings
* Various smoke VFX tweaked
* Ammo boxes all now have unique sprites

## Character Mechanics
* Jump cooldown after climbing decreased

## Weapon Mechanics
* Adjusted pickup messages and tags to be more consistent throughout
* Shotgun shell drops are now a set amount instead of random
* Increased effectiveness of Demon Level on recoil reduction

# + CHARACTERS AND WEAPONS +
### Melee
* Stun threshold of punching increased
* Fixed being unable to throw flashbangs if out of frag grenades
* Grenade throwing updated to be consistent with guns
## THE ISRAELI - OFEK AVADAAN
### Desert Eagle
* Placeholder animation for unloading/post repair added

## Version 0.0.8 - October 6, 2024
# + GAMEPLAY +
## General 
* Ammo Capacities added. All ammo types and all Gear now has a limited capacity you can carry.
* Rucksack added. Extra ammo and gear can be stored.
* MOLLE Manager added. Pouches can be found to expand your ammo and gear capacities. This manages them. See the Manual for more details.
* Marine Corpses can now be searched with Use to find MOLLE Pouches.
* Spawn Chance CVars added for Ammo, Medical Gear and Tactical Gear!
* Ammo no longer spawns loose. Instead it now spawns in boxes and cases that must be manually opened with Use.
* Backpacks now drop a lot of different ammo and MOLLE Pouches
* Barrel explosion shrapnel removed
* Barrel health increased
* Barrel debris added
* Barrel debris CVar added
* Updated barrel sprites
* Liquid splashes added
* Keycard sprites updated
* Various explosion VFX updated to be dynamic with hitting ceilings, walls and floors
* Various explosion SFX updated and dynamic with hitting targets, walls or liquids
* Bullet puff VFX updated to be dynamic with hitting ceilings, walls and floors
* Fixed explosion quake effects being cut short
* M_DOOM updated

## Character Mechanics
* SFX added to getting hit with armor
* Pain Response mechanic added. Taking damage throws your view around. Characters with high Cyber Level will see screen static.
* Climbing now puts jumping on a cooldown
* Climbing now costs stamina
* Climbing height is now calculated from view height (character height)
* Jumping is now always enabled, regardlesss of map settings
* Characters with high Cyber Level who take Electric or Plasma damage will now have their HUDs disrupted for a duration and take damage to all Body Parts. Plasma does set damage, Electric does less and random damage
* Head Damage scaling added: Pain Response increases
* Leg Damage scaling speed penalty removed
* Body Damage Severe stamina drain increased
* Weight speed penalties increased
* Weight stamina drain penalty increased
* Exhausted speed penalty increased
* Exhausted recoil penalty increased
* Corruption now very slowly drains over time
* Corruption now ???
* Hobbled penalties increased
* Fixed jumping directly out of climbing
* Fixed jumping stamina cost being tied to the jump button instead of actually jumping


## Gear
# Medical Gear
* Medikit sprite updated
* Medical scanner added that passively goes and displays detailed health information
* Medikits now heal more effectively (shorter heal intervals) when holding still and less effectively (longer heal intervals) when running. Walking heals at the normal rate
* Berserk health regeneration is now constant instead of in 1 second intervals
* Berserk health regeneration base speed increased
* Berserk now greatly increases Melee attack speed
# Tactical Gear
* Claymore puffs removed

## HUD 
* Weapon Maintenance Icon now changes with Weapon Type
* Weapon Maintenance now displays weapon information
* Familiarity Bar moved to above ammo display
* Old Familiarity bar is now Recoil and Stamina Pulse
* Door locked messages updated
* Keycard icons updated
* Various positions tweaked
* Demon Level bar removed
* Various values will now turn Green when full
* Flashbang amount added

## Weapon Mechanics
* Added SFX when firing below 35% ammo
* Added SFX to firing last shot
* Pistol Mag weight increased
* Shotgun Shell weight increased
* CAWS Mag weight increased
* Sniper Mag weight increased
* Dropped guns with high enough Heat will now emit smoke
* Familiarity rewritten to use Levels instead of flat Familiarity amount. 
* SFX added to gaining Familiarity Level
* Amount of Familiarity for Familiarity Level increases with Familiarity Level
* Liquid cooling SFX added
* Single shot weapons Heat generation increased
* Shotgun heat generation increased
* Shotgun Recoil Pulse increased
* Weapon Maintenance Gun Part health cap is now increased by Familiarity Level
* Weapon Maintenance Gun Part replacement speed now scales with Cyber Level and Familiarity Level
* Heat dispersion speed decreased
* Heat dispersion speed now increases while preforming Weapon Maintenance 
* Range damage falloff thresholds decreased
* Fixed various pointless pre-state delays
* Mag retrieve times adjusted to be more consistent across gun types

# + CHARACTERS AND WEAPONS +
* Now start with 1 Flashbang
* Casings added
* Various SFX updated
* Shotguns now spawn with a full magazine but unloaded
* Pistols finished and fully sprited now
* Pistols now always start at Familiarity Level 3
* Reload speeds adjusted to be more consistent across gun types
* All characters weapons will now be highlighted with high enough Familiarity
* Familiarity now also have a dynamic light attached
### Melee
* Stun mechanic added. Dealing so much damage based on the target's current health will stun the target
* Animation is now dynamic with hitting enemy or whiffing
* Kick chance reduced to 10%
* Athleticism now factors into Max Damage
* Berserk now adds Armor Piercing 
* Stimmed damage bonus increased to 33% from 25%
* Familiarity gain decreased from hitting nothing
* Familiarity gain increased from hitting enemies
* Fixed Berserk causing Melee damage to be reduced at high enough Demon Level

## THE RUSSIAN - 'HELGA'
### SVD
* Sprites un-JPG'd
* Firing sprite updated
### RPG-7
* Direct hit now ignores armor
* Initial explosion damage increased
* Initial explosion damage type changed to Piercing
* Initial explosion now ignores armor
* Secondary explosion damage decreased
* Secondary explosion radius decreased

## THE AMERICAN - JOHN SMITH
* Voice updated
* Favored Weapon is now Pistol
* Combat Software crit chance increased
* Combat Software crit SFX added
* Combat Software crit now does Piercing damage
* Combat Software crit now forces pain
* Combat Software crit now always gibbs
* Combat Software crit now ignores armor
### SW1911
* Changed to Kimber 1911
  * Purely visual
### M4A1 w/ M203
* Reload/Unload animation updated
### XM500
* Changed to FD338
  * Semi auto Sniper chambed in .338 Lapua Magnum
  * Reduced damage and range
  * New sprites 
### China Lake
* Projectile speed increased
* Projectile arming time decreased
* Tweaked projectile physics
* Increased reload speed
### M60
* Sprites updated

## THE GERMAN - WILHELM STROMHEIM
* Pain sounds removed
* Minimap removed
* Weapon sprites updated with HANDS
* Melee sprites updated
* Prefire delay reduced
* HUD adjusted to increase readability 
* HUD Sniper icon added
* HUD weapon icons now change color based on load and ammo status
* Reduced rate falling landing sound will play
* Step height reduced to vanilla
* Center HUD bars now hidden on non-guns
* Center HUD bars are now thinner
* Center HUD sized and alpha reduced
* Center HUD now shows Recoil and Stamina Pulse
* Center HUD bar positions adjusted
* Fixed some weapons having the wrong weight
### P99C
* Changed to MK23
  * Better in every single way 
### MP7
* Changed to UMP45
  * Higher damage, lower RoF, lower mag 
### CAWS 
* Sprites updated
* Damage increased
* Fixed rate of fire being much higher than intended 
### XM8 
* Changed to G36KA4
  * Largely visual 
### WA2000
* Damage increased
* Zoom now fixed at 5x
### M320
* Sprites updated
* Homing removed
* Rocket now accelerates after arming fully
* Fixed loading multiple rockets
* Added initial piercing explosion

## THE ISRAELI - OFEK AVADAAN
* Melee sprites updated
* Character height/view height increased
* Fixed some weapons having the wrong weight
* Fixed some Hellfire explosives dealing the wrong DamageType
### Uzi
* Reload sprites updated
### Remington 870
* Fixed delay after firing before you could pump
### ACE-23
* Reload animation finished
* Firing sprite updated
### M4A1 Kala Sa'ar
* Changed to Tavor 7
  * Higher damage, lower RoF, lower mag, no scope 
### Fireball
* Direct damage increased

## THE ITALIAN - FATHER WARD
* Character height/view height increased
### Cross
* Replaced with proper Melee, with the Cross being special actions.
* Cross use now has limited energy
* Cleansing Corruption is no longer constantly held. Instead it is now a lengthy ritual that clears a chunk of Corruption.
* Cross damage increased
* Cross damage rate increased
* Cross range decreased
* Cross effective angle decreased
* Cross now ignores armor
### Bible
* Capacity cap is now 2
* Greatly increased hit amount
### ARX-200
* Sprites udpated


## Version 0.0.7 - Jan 21, 2024
# + GAMEPLAY +
## General
* Lots of codebase overhauls and rewrites. Some subtle changes in weapons might be noticed, but won't be noted in detail. Mod is less janky overall and more consistent 
* Decorations made shootable and dynamic, with destruction states, brightmaps and dynamic lights!
* Debris CVar added for above decorations.
* Key pickup sound added
* Melee is now defaulted to on weapon drop instead of Ammo Manager
* M_DOOM now fits on screen (IMPORTANT)
* Foley sounds added to retrieving a mag, putting a mag away, pulling a grenade and weapon movement
* README updated with recent information
* Manual added.
* Various other sounds updated

## Character Mechanics
* Athleticism characteristic added. Represents the overall (organic) physical fitness of the character, and determines various physical attributes. Ranges from 0 to 5.
* Overheal updated. In addition to Body Part regeneration, Overhealing increases Max Carry Weight, melee damage, Max Stamina, Stamina Recovery and reduces recoil. All effects scale with what percent of your Max Health you are overhealed, and are reduced by Demon Level
* Max Health calculation updated. Cyber Level scaling increased after a certain Cyber Level threshold
  * Yes this was done purely so Wilhelm's HP is 200 and not 194 
* Max Stamina is now calculated based on Athleticism, Cyber Level and Demon Level. Human characters largely unchanged, but non-humans have much less Stamina than before
* Stamina drain from jumping, running and sliding increased
* Stamina recovery reduced
* Stamina recovery from holding still increased
* Stamina recovery from holding still now triggers much easier
* Removed inability to run while Exhausted
* Bodily Damage now has scaling penalties. Penalties start at and scale from 85%. Status message thresholds altered to reflect this.
  * Body: Max Stamina reduction
  * Arms: Recoil increase
  * Legs: Max Carry Weight reduction 
* Severe Body damage now causes Stamina bleed
* Weight Penalties to speed rewritten to be smoother overall
* Corruption threshold scaling from Demon Level reduced
* Fixed Weight Penalties to movement speed, Stamina use and Stamina recovery being non-functional
  * They literally never worked outside of speed 

## Weapon Mechanics
* Damage calculation simplified slightly. Damage is around the same overall, but with slightly higher minimum damage rolls
* Range Damage Fallout mechanic added. The further you are from the target, the less damage you do based on the Range of the weapon
  * Less than 50% Range: Full damage
  * 50+% Range: 75% damage
  * 75+% Range: 66% damage
  * 100+% Range: 50% damage
  * 200+% Range: 33% damage
  * 300+% Range : 10% damage
  * 400+% Range: No damage
* Grenade Delay added. Delays the throwing of a grenade based on weapon's size.
* Recoil Pulse mechanic added. As you fire, Recoil Pulse increases, multiplying recoil. It increases faster the faster you are moving. It decays slowly over time, and quickly when holding still and holding the weapon idle.
* Stamina Pulse mechanic added. Like above, but for stamina use.
* Pistol Canceling mechanic added. You can interrupt Weapon Maintenance and Reloading by quick drawing your pistol. You can only interrupt reloads before you retrieve the new magazine, and interrupting Weapon Maintenance causes damage to the gun and all Gun Parts. Replacing a part and finishing Weapon Maintenance cannot be Pistol Canceled
* Weapon Maintenance tic rate is now static
* Weapon Maintenance will now skip checking a Gun Part if it's HP is above 95%
* Weapon Maintenance will now quick check a Gun Part if it's HP is above 85%
* Weapon Maintenance will now loop back to Cleaning after Part Repair if minimum possible Condition has changed
* Weapon Maintenance finishing speed increased
* Cleaning amount reduced
* Cleaning amount Condition scaling increased
* Cleaning amount Cyber Level scaling decreased
* Part Repair amount reduced
* Part Repair amount Cyber Level scaling decreased
* Part Repair amount Condition scaling increased
* Part Repair amount now scales with combined Gun Part HP
* Relacing the Barrel now reduces Heat
* Familiarity gain from Weapon Maintenance reduced
* Familiarity is now very slowly gained while weapon is weilded
* Stamina drain from firing is now reduced by Cyber Level
* Gun Part damage Condition scaling reduced
* Scaling effect added to Trigger: Trigger delay
* Threshold effect added to Receiver: Reduced Range
* Recoil scaling from Cyber Level decreased
* Pistol select speed increased, and they ignore select speed calculations
* Ammo dropping speed is now uniform and faster overall
* Pre-repair delay (after animation) is now uniform
* Various animations updated, tweaked or fixed
* Weapons with scopes now clear that scope on more functions
* Switching speed now scales with Stamina percent
* Heat gain decreased
* Heat dispersion decreased
* Heat now disperses faster if weapon is dropped
* Heat now disperses much MUCH faster if a weapon is dropped in a liquid
* Fixed climbing while preforming Weapon Maintenance sticking the Maintenance status but being in the Ready state
* Fixed Stamina drain while firing being largely non-functional
* Fixed Receiver damage not scaling with Condition
* Fixed Condition being able to go negative
* Fixed Heat being able to go negative
* Fixed Condition being set to a very low amount when cleaning
* Fixed Condition increasing over time while cleaning
* Fixed skipping Barrel repairing restarting Barrel repairing
* Fixed various guns ignoring bolt/mag stuck mechanics relating to Condition and Gun Part health
* Fixed various guns having extra delay on various actions for no reason
* Fixed being unable to attempt to Quick Unjam when out of spare ammo
* Fixed being unable to quick throw Stun Grenades when out of Frag Grenades
* Fixed various pump actions being pumpable while reloading

## Gear
* Tags removed
* Gear mapped to Main Fire and Alt Fire keys can now be auto fire dropped
* Gear switch speeds are now uniform
* Gear dropping speeds are now uniform and increased overall
* Gear print messages now clear and return control faster
* Magic based items with durations now apply Corruption over time for the duration they are active
* Magic based items with durations now have a minimum duration of 30 seconds and a maximum duration of 180 seconds
* Magic based items Corruption on use decreased

# Medical Gear
* Medikit use progress bar added
* Stimpacks no longer heal Bodily Damage
* Stimpacks now remove Exhausted
* Stimpacks now grant a 90 second buff that increases Stamina Recovery and melee damage, and reduces recoil
* Screen blend from Radsuit updated
* Radsuit now has a rebreather overlay
* Fixed Armor Bonus gained from Armor Plates not scaling with Demon Level
* Armor amount cap increased to 999 from 200
* Blue Potion and Armor Plate failure chance now scales with amount held, going down as you hold more. Failure chance can not go lower than 15% of Cyber Level
* Blue Potions can be drunk faster by looking up as you chug
* Armor Plates can be...monched? Used? Applied? Applied. faster by looking down as you apply
* Berserk melee damage bonus is now set to x10, reduced by Demon Level
* Berserk damage reduction increased to 33% from 25%
* Berserk health regeneration increased to 3 hp/sec from 2 hp/sec
* Berserk Carry Weight reduction reduced and capped
* Berserk weapon throw chance is now only checked after a rage shake
* Berserk weapon throw chance removed when Demon Level is 15 or higher
* Berserk weapon rage shake Condition gain reduced and randomized
* Berserk weapon rage shake now damages Gun Parts

# Tactical Gear
* Claymore code rewritten and they actually function as expected now
* Claymore setting animation removed
* Claymore setting progress bar added
* Claymores are now shootable, and go off if shot

# Magic Gear
* Magic Gear failure chance removed
* Reduced scaling from Cyber Level
* Invulnerability sprite updated
* Invulnerability now prevents Stamina use while in effect
* Invulnerability now grants immunity to damage thrust
* Blur Ring is now properly refered to as a Ring and not a Sphere
* Blur Ring now has a grey scale effect
* Megasphere sprite updated
* Megasphere HP bonus now scales with Cyber Level
* Megasphere HP bonus can no longer be less than 25
* Megasphere now causes Corruption

## HUD 
* Lots of alignment, consistency and position tweaks
* Armor indicators no longer blank out when armor is depleted 
* Stamina status levels updated to Optimal, green Fine, yellow Fine, Tired and Exhausted
* Body Part status now changes color with Body Part HP
* Body Part status levels updated to Perfect, Good, Fine, Wounded, Severe
* Condition status levels updated to Perfect, Great, Passable, Substandard, Terrible
* Condition icon now turns white when Condition is close to its possible minimum
* Gun Part status levels updated to Perfect, Great, Fine, Worn, Broken
* Gun Part icons now turn red when part is broken
* Gun Part icons now turn orange when jammed
* Weapon Maintenance display layout updated
* Powerup timers made more readable
* Corruption/Demon Level bar updated to be more readable, and include Blue Bonus amount

# + CHARACTERS AND WEAPONS +
* Melee overhauled. 
  * No longer has an Alt Fire
  * Damage is now calculated based on Demon Level and Cyber Level
  * Attack speed is uniform
  * Pre-attack delay is now calculated based on Athleticism
  * Costs Stamina to attack, and delay scales with Stamina to Max Stamina ratio
  * Can be held to auto fire
  * Has 3 levels of Familiarity with the follwing effects
    * Lower Stamina use
    * 10% chance of doing a 2.5x damage kick
    * Faster attack speed
* Dry-fire sound added
* Pump action weapons no longer lose a round when preforming Weapon Maintenance
* Pump action weapons pre and post Weapon Maintenance animations removed
* Shotgun shell spawn amounts reduced from all sources
* Sniper Mag pickup sound updated
* MG ammo types merged into one
* Archmage BFG removed. Instead, a Machine Gun and Launcher pickup will both spawn on BFG spawns
* Frag Grenades no longer spawn on Clip and Shell spawns 
* Frag Grenade spawn chance on ClipBox and Shellbox spawns reduced
* Clip spawns are now less likley to spawn a new SMG
* Clip Box spawns are now less likely to spawn Pistol Magazines
* Cell spawns will now be replaced with either a MG Belt, or various magazines
* Cell Pack spawns will now be a MG Belt and various magazines in addition to Classified Ammo
  * Classified Ammo is currently unused, but is weightless and has no impact on any gameplay  

## THE RUSSIAN - 'HELGA'
* Athleticism is 5
### TOZ-34
* Improved effectiveness of faster reloads from Familiarity
### AK-12 
* Weight increased
* Range decreased
* Bullet mass decreased
* Bullet velocity increased
### PKP
* Weight increased
* Bullet velocity increased

## THE AMERICAN - JOHN SMITH
* Athleticism is 3
* Field Enhancement: Combat Software crit bonus increased to x2 from x1.5
### Vector
* Increased effectiveness of Familarity bonus on reload speed
### Mossberg
* Reload speed decreased
### M4A1 with M203
* Now has a 5% chance to spawn with the M203 loaded
* Can no longer fire bullets while M203 is selected
* M203 HUD information added
* M203 toggle speed increased
* M203 loading speed reduced
* Bullet velocity increased
* Fixed being unable to fire the M203 if the M4's magazine was empty
### China Lake
* Reload speed increased
### M60E4
* Length decreased
* Weight decreased

## THE GERMAN - WILHELM STROHEIM
* Athleticism is 1
* Additional bars added to HUD
* Stamina display on HUD is now a precent
* Various delays are now uniform
* Fixed magic pixel on Condition bar
* Stamina and Condition bars will now indicate the highest/lowest possible value
* Damage Type Resistances and Weaknesses updated to match LEGION's Cyber creatures
* Removed 25% blanket Damage Resistance
* Familarity bonus on reload speed now effects delays
### CAWS
* Ammo type updated to CAWS Magazine
  * Weight the same as 10 Shotgun Shells
  * Cannot be dropped; Shotgun Shells will be dropped instead 
* Alt Fire function added: CAWS Magazine loader
  * Converts 10 Shotgun Shells into a CAWS Magazine
* Now spawns with a full magazine
* Can now be Unloaded  
* Pellet count increased to 10 from 8
### XM8
* Range decreased
### M320
* Added pre-fire delay
### MG4
* Added pre-fire delay
* Bullet velocity decreased
* Rate of Fire decreased

## THE ISRAELI - OFEK AVADAAN
* Athleticism is 2
* Hellfire Claymores added
  * Looks all around, doesn't shoot pellets, largely explosive radius and lights the ground on fire.
* Hellfire C4 added
  * Launches small fireballs on detonation
* Hellfire Grenade shrapnel amount reduced
* Field Enhancement: Hellfire Rounds functionality updated. Instead of converting damage to Hellfire and causing small explosions, deals an extra 25% Hellfire damage with a small explosion on top of the weapon's normal damage
### Melee
* Fixed sprite conflict with LEGION
### Desert Eagle
* Fixed reload speed being unaffected by Familiarity
### Remington 
* Reload speed decreased
* Animations tweaked
### ACE
* Rate of fire decreased
### Fireball
* No longer gains or loses Condition
* No longer gains Familiarity
* No longer gets Mana from Launcher pickups
* Round count now matches Mana, removing Reload prompt
* Casting time increased
* Pistol Cancel window added to casting
* Mana cap is now 9
* Shrapnel removed from explosion
* Fixed Fireball not doing Fire damage
### Negev
* Reload is now shortened when reloading from empty
* Range decreased
* Bullet velocity increased

## THE ITALIAN - FATHER WARD
* Athleticism is 0
* No longer gets Crosses from Launcher pickups
* Field Enhancement: Blessed Ammo functionality updated. Instead of converting damage to Holy, deals an extra 25% Holy damage on top of the weapon's normal damage
### Cross
* Switching speed increased
* Fire delay reduced
### 92FS
* Reload speed increased
### PMX
* Reload speed increased
* Bolt pull speed increased
* Range reduced to 50m from 150m
* Fixed missing Familiarity bonus to reload speed
### M4
* Quick Unjam added
### ARX-160
* Reload speed increased
* Bolt pull speed increased
* Bolt pull animation tweaked
### ARX-200
* Reload speed increased
* Bolt pull speed increased
* Bolt pull animation tweaked
### Bible
* Pistol Cancel window added to recital
* Recital speed reduced
* Round count now matches spare Cross count, removing Reload prompt
* No longer gains or loses Condition
* No longer gains Familiarity
### Minimi
* Magazine capacity reduced to 100 from 200
* Length decreased
* Fixed range being much less than intended