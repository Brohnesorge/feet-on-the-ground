# - CHARACTER MECHANICS -
Characters have base 50 HP and base 500 Stamina. HP scales up with Demon Level and Cyber Level. Stamina scales up with Demon Level, Cyber Level and Athleticism. 

Characters can perform a slide by crouching while running, if speed is high enough.

### Cyber Level, Demon Level and Athleticism
These three stats represent the physical properties of the character, and factor into a number of calculations
* **Cyber Level** is how cybernized a character is, or what percent of them is machine and how much is human. It affects HP, Max Stamina, Melee damage, Recoil and Bodily Damage. It reduces the effectiveness of Magic Items, but increases the potentcy of healing.
* **Demon Level** is how much of your essence is demonic. It effects HP, Max Stamina, Melee damage and Recoil. It increases the effectiveness of Magic Items, but reduces the potentcy of healing. Higher levels alert enemies to your presence; they can smell your demonic blood, but know that you are still largely human. If Demon Level reaches 100, you become a Demon resulting in a Game Over.
* **Athleticism** is how physically fit a character is. It can be from 0 (Unhealthy, out of shape) to 5 (peak human performance). Athleticism effects Max Stamina and Melee attack speed.

### Pain Response 
When taking damage, you will have your view thrown based on how much damage was taken. The amount of view throw is based on the damage taken compared to Max Health, Cyber Level and Athleticism. With high enough Cyber Level, Electric and Plasma attacks will also cause HUD disruptions and deal damage to all Body Parts (see Bodily Damage below)

### Movement 
You can mantle and climb ledges by holding Jump. This consumes Stamina and the speed is effected by various factors such as Athleticsim and height. You can climb after jumping and while falling. After climbing, you are unable to jump for a second, but can still move and climb.

### Stamina
Stamina is tied to many calculations, and maintaining it is important to longer play. Most things scale off of current Stamina compared to Max Stamina. If completely out of Stamina and you move too fast, you become **EXHAUSTED**, resulting in a heavy speed penality for a few seconds. Stamina recovers over time when moving slowly, and recovers faster when standing still. Stamina drains when firing weapons, using melee, jumping or running.

### Weight
All characters have a base carry capacity of 200 pounds. A backpack raises this to 300 pounds. Weapons, ammo and items all have weight, and you take various drawbacks based on how heavy you are
* 0% - 24% - **NONE** - No penalties
* 25% - 49% - **LIGHT** - 110% Stamina use, 90% Stamina recovery speed, 95% movement speed
* 50% - 74% - **HEAVY** - 125% Stamina use, 75% Stamina recovery speed, 85% movement speed
* 75% - 99% - **CRUSHING** - 150% Stamina use, 50% Stamina recovery speed, 70% movement speed
* 100+% - **HOBBLED** - 200% Stamina use, 25% Stamina recovery speed, 50% movement speed

### Bodily Damage
When hit, a body part will also take damage based on the damage of the attack. Cyber Level reduces Bodily Damage taken. Body parts have a scaling penalty starting at 85% Part HP. At 50% Part HP, that Body Part is **WOUNDED**, causing an additional penalty. At 25% Part HP, that Body Part is **SEVERE**, inflicting a worse penalty. The Body Parts, the chance they take damage and their penalties are. 
* HEAD - 10% hit chance
  - Increases Pain Response 
  - **WOUNDED**: Obscured vision 
  - **SEVERE**: Worse obstruction
* BODY - 50% hit chance. Damage taken halved when wearing armor.
  - Max Stamina reduced 
  - **WOUNDED**: Bleed 
  - **SEVERE**: Faster bleed and steady Stamina drain
* ARMS - 15% hit chance
  - Recoil increased 
  - **WOUNDED**:  Recoil is quadrupled 
  - **SEVERE**: No additional effect  
* LEGS - 25% hit chance
  - Carrying Capacity reduced 
  - **WOUNDED**: Movement speed reduced by 1/3 
  - **SEVERE**: Movement speed reduced by 2/3

# - ITEMS AND GEAR -
Items and powerups are not used on pickup, instead you must select the relevant Gear and use them manually.

## Slot 8: TACTICAL GEAR
Tactical gear is things like flashbangs and explosives. They are typically found on Rocket and Rocket Box spawns.
* Main Fire: **Claymore**. Sets an explosive mine. When a target enters its range within a 90 degree cone, it detonates, spraying an extreme amount of steel balls. Can be shot to trigger detonation early.
* Alt Fire: **C4**. Throws a demo charge. Demo charges deal extremely high explosive damage and can destroy doors and walls. Does not detonate when shot.
* Reload: **Detonator**. Detonates all set C4.
* Zoom: Draw sidearm.
* User 1: **Frag Grenade**. Throws a fragmentation grenade. Explodes and sprays shrapnel all around.
* User 2: **Stun Grenade**. Throws a flashbang, stunning all enemies in a large radius. Enemies immune to pain are unaffected.
* User 3: Nothing.
* User 4: Toggles Drop Mode. Drops the gear instead of using it.
## Slot 9: MEDICAL GEAR
Medical gear is healing items, as well as some other gear.
* Main Fire: **Medikit**. Long to apply, and heals a great amount of health over time. Duration, length of healing tics and healing amount are increased by Cyber Level and decreased by Demon Level. Duration increases the lower your health. Heals Body Parts by 25 HP, and an additional 1 HP per healing tic. Duration rapidly drains when Health is full.
* Alt Fire: **Stimpack**. Quick, small heal. Does not heal Bodily Damage. Heal increases with Cyber Level, and decreases with Demon Level. Regardless of heal amount, user is Stimmed for 90 seconds. Being Stimmed increases Stamina recovery and melee damage, increases climb speed and reduces recoil and pain response.
* Reload: **Blue Potion Flask**. 1 Health heal increased by Demon Level, capable of overhealing. Has a chance to fail based on Cyber Level, but chance is reduced by amount of Blue Potion owned, to a minimum based on Cyber Level. Heals 1 point of Bodily Damage. Causes Corruption (See Magic Gear below). Being overhealed grants many benefits, increased by what percent of your maximum Health you are overhealed and decreased by Demon Level. Being overhealed regenerates Bodily Damage, recovers Stamina over time, increases Max Stamina, increases Carry Capacity, reduces recoil and increases melee damage.
* Zoom: Draw Sidearm
* User 1: **Armor Bonus**. 1 Armor recovery increased by Demon Level, capable of 'overhealing' armor. Has a chance to fail based on Cyber Level, but chance is reduced by amount of Blue Potion owned, to a minimum based on Cyber Level. Causes Corruption (see Magic Gear below). Cannot be used if you have no armor, and provides no benefits besides increasing armor durability above maximum.
* User 2: **Berserk**. Heals a large amount of health, and causes the user to go berserk. While berserked, deal multiplied melee damage, reduced by Demon Level, rapidly recover Stamina, suffer less damage, recovery health over time, completely eliminates recoil, reduces weight and rapidly regenerate Bodily Damage. While berserked, you cannot perform Weapon Maintenance, you regularly jostle and damage your weapon, and have a small chance to throw it in favor of using melee. Having a high enough Demon Level removes the chance to discard your weapon. Duration increases with Demon Level, and decreases with Cyber Level. Causes a large amount of Corruption (see Magic Gear below).
* User 3: **Hazmat Suit**. Protects from environmental damage for 5 minutes.
* User 4: Toggles Drop Mode. Drops the gear instead of using it.
## Slot 0: MAGIC GEAR
Magic Gear is magic items, powerups and artifacts. Using Magic items causes Corruption on both use and while they are in effect. Too much Corruption results in gaining a Demon Level. The amount of Corruption required to gain a Demon Level increases as Demon Level does. Corruption cannot be cleansed, but is reset upon gaining a Demon Level.
* Main Fire: **Invulnerability Sphere**. Grants complete invincibility. Duration increases with Demon Level, and decreases with Cyber Level. Causes a large amount of Corruption.
* Alt Fire: **Blur Ring**. Makes you harder to detect, and greatly reduces damage taken from elements and magic. Duration increases with Demon Level, and decreases with Cyber Level. Causes a large amount of Corruption.
* Reload: **Megasphere**. Overheals a large amount, and grants magic armor. Overheal amount increases with Demon Level, and decreases with Cyber Level. Causes a moderate amount of Corruption.
* Zoom: Draw sidearm.
* User 1: **Computer map**. Reveals the map of the level.
* User 2: Drop Night Vision Goggles. NVGs are used with F, by default.
* User 3: Drop Repair Kit. Used while performing Weapon Maintenance.
* User 4: Toggle Drop Mode. Drops the gear instead of using it.
## Slot 1: RUCKSACK 
Used for storing ammo and gear past your capacities. Lower priority than melee.
* Main Fire: Retreive selected item
* Alt Fire: Store selected item
* Reload: Cycle to next item
* Zoom: Cycle to previous item
* User 1: Cycle to next compartment
* User 2: Cycle to previous compartment
* User 3: Drop selected item. Drops from the rucksack first, then from your carried inventory.
* User 4: Dump all items from the rucksack.
## Slot 1: MOLLE
Manages the pouches that determine your gear capacities. 
* Main Fire: Drop selected pouch 
* Reload: Cycle to next pouch 
* Zoom: Cycle to previous pouch

The pouches, their governed ammo types and amounts and how many you start with are listed below. The maximum amount of slots is 13.
The starting pouches CAN be dropped to make room for new pouches.
* Small Mag: Pistol (2), SMG (2). Start with 2.
* Large Mag: AR (3), Sniper (2), CAWS (2). Start with 2.
* Shell Bag: Shells (20). Start with 2.
* Grenades: Frag Grenade (2), Flashbang (2), 40mm (2), PIKE (2). Start with 2.
* MG Mag: Machine Gun (1). Start with 1.
* Medical: Medikit (1), Stimpack (5). Start with 1.
* Tactical: Claymore (2), C4 (2). Start with 1.

The minimum capacity of the above items is 0. The exceptions are Pistol Mags (1), SMG Mags (1), Frag Grenades (1) and Shotgun Shells (10).

# - WEAPON MECHANICS -

Weapons have weight, heat, range based damage fall off, operate on magazines, sustain damage with use and a largely universal control scheme

* Main Fire: Fires the weapon.
* Alt Fire: Performs a function. Usually nothing. Exceptions noted in weapon list. You can bring up a hint message displaying controls by holding Use and pressing this.
* Reload: Reloads. Does not prevent you from reloading on a full magazine, wasting it. If the weapon is jammed, attempts to quickly unjam with a high rate of failure.
* Zoom: Draws sidearm and drops weapon. Reloading and Weapon Maintenance can be interrupted by drawing the sidearm.
* User 1: Throws a Frag Grenade. Can be held down to throw a flashbang instead.
* User 2: Weapon Maintenance. See below.
* User 3: Drop ammo. Drops spare ammo of the weapon.
* User 4: Unload and drop weapon. Unloads a full magazine, adding the ammo to your inventory and drops the weapon. If the magazine isn't full, just drops the weapon.

Many factors contribute to damage, such as round speed, round weight, range and type of round. Firing the gun consumes Stamina, based on the weapon's weight, and causes recoil, based on weapon's weight and length. Stamina used and recoil grow as the weapon is fired. These growth values increase greatly with current movement speed, and decay back to normal over time. The rate of decay is increased by holding still, and greatly increased by holding still and doing nothing with the weapon.

Weapons generate Heat when fired. Heat disperses over time slowly, increasing in rate when the weapon is dropped. Dropping the weapon in a liquid sharply increases heat dispersion. If Heat rises to above 60%, the gun starts gaining Condition and taking Gun Part damage rapidly until it cools back down to below 60%.

The Range of a weapon effects both its spread and damage. Damage falls off with range at the following rate

  * Less than 50% gun range: Full damage
  * 50+% gun range: 75% damage
  * 75+% gun range: 66% damage
  * 100+% gun range: 50% damage
  * 200+% gun range: 33% damage
  * 300+% gun range: 10% damage
  * 400+% gun range: No damage

## Weapon Maintenance
As a weapon is fired, held, owned, dropped and operated, it gains Condition. Condition reduces the performance of the gun in many ways, scaling with Max Condition. Max Condition is the same across all weapons. Each part of the gun (Gun Part) has its own HP, and these have various scaling and threshold effects, similar to Body Parts and Bodily Damage. Firing the gun causes Gun Part damage. Failure to fire chance and jam chance increase with Condition. Jams cause heavy Gun Part damage, based on the nature of the jam.

Performing Weapon Maintenance reduces Condition, clears jams and repairs Gun Parts. The amount reduced/repaired increases with Cyber Level, decreases with Demon Level and scales with Condition. Gun Parts cannot be repaired above 85% HP, and cannot be repaired at all if they fall below 35% HP. Gun Parts can be replaced with Repair Kits, resetting their HP to 100% and removing a large portion of Condition. The lowest Condition can be scales with the total of all Gun Parts.

Condition cleaning and Gun Part repair happen one step at a time. While repairing, you can press Main Fire to skip ahead a step or Alt Fire to skip to the end of Maintenance. While checking a Gun Part, you can hold Reload to replace the part. Replacing a part takes a few seconds and consumes a Repair Kit. If a part's HP is too low, it will be replaced automatically.

Gun Parts have a scaling effects, as well as additional effects at 75%, 50% and 25% Part HP
* BARREL
  * Increases spread  
  * 75% - Spread scaling increases
  * 50% - Spread scaling increases further
  * 25% - Spread scaling increases even further
* LOWER
  * Increases Trigger delay
  * 75% - Trigger failure chance increases
  * 50% - Trigger failure chance increases
  * 25% - Trigger failure chance increases
* UPPER 
  * No scaling effect
  * 75% - Decreases bolt working and pumping speed 
  * 50% - Decreases bolt working and pumping speed further
  * 25% - Decreases bolt working and pumping speed even further
* CHAMBER
  * Reduces damage
  * 75% - Decreases range
  * 50% - Decreases range further
  * 25% - Decreases range even further

## Familiarity
As a weapon is used and maintained, it gains Familiarity. Familiarity is kept on that unique weapon, not all weapons of that type. Familiarity is increased by firing, reloading and cycling the action, increased very slowly over time, and increased over time while preforming Weapon Maintenance. Familiarity gains a level every 2000, and maxes out at level 5. Familiarity provides the following benefits
* Level 1: Reduces recoil by 30%
* Level 2: Increases reload speed and reduces Grenade delay
* Level 3: Reduces spread by 20% and reduces Recoil/Stamina Pulse by 33%
* Level 4: Reduces jam chance by 20% and reduces heat generation by 50%
* Level 5: Reduces Condition gain from by 75%; activates character's Field Proficiency

# - CHARACTER AND WEAPON STATS -
Characters will be listed below, with the following stats. Most characters start with Lv3 Familiarity in their sidearm
* Cyber Level
* Demon Level
* Athleticism
* Base Speed
* Field Proficiency
* Specialty - The weapon the character specializes in, starting with Lv1 Familiarity
* Unique - Any unique traits 

Their weapons will be listed below them, with the following stats 
* Slot
* Ammo Type
* Damage Type
* Range
* Capacity
* Type

## Melee 
* Slot: 1
* Costs Stamina 
* Dealing more than 35% of an enemy's health will stun them breifly
* Damage scales with Cyber Level, Stamina, Demon Level, Arm HP and Athleticism
* Speed scales with Familiarity Level, Stamina and Athleticism
* 3 Levels of Familiarity
  * Level 1: Lower Stamina use 
  * Level 2: 10% chance to Kick, dealing increased damage and stunning more easily
  * Level 3: Much faster attack speed

## 'Helga', The Russian
* Cyber Level: 0
* Demon Level: 0
* Athleticism: 5
* Base Speed: 0.7
* Field Proficiency: Weapon Savant. Much faster reloads
* Specialty: Sniper rifle
* Unique: None
### - Weapons -
* Makarov
  * Slot: None
  * Ammo Type: Pistol
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 12
  * Type: Sidearm
* PP-2000
  * Slot: 2
  * Ammo Type: SMG
  * Damage Type: SmallArms
  * Range: 200m
  * Capacity: 44
  * Type: SMG
* TOZ-34
  * Slot: 3
  * Ammo Type: Shotgun Shell
  * Damage Type: SmallArms
  * Range: 40m
  * Capacity: 2
  * Type: Shotgun
  * Alt Function: Fires both barrels at once
* AK-12
  * Slot: 4
  * Ammo Type: AR
  * Damage Type: LongArms
  * Range: 500m
  * Capacity: 30
  * Type: Assault Rifle
* SVD
  * Slot: 4 (Low priority)
  * Ammo Type: Sniper
  * Damage Type: Long Arms
  * Range: 830m
  * Capacity: 10
  * Type: Sniper Rifle
  * Alt Function: Toggles 4x scope
* RPG-7
  * Slot: 5
  * Ammo Type: Rocket
  * Capacity: 1
  * Type: Launcher
  * Unique: Fires an extremely damaging HEAT capable of OHKOing most enemies
* PKP Pecheneg
  * Slot: 6
  * Ammo Type: LMG
  * Damage Type: LongArms
  * Range: 1500m
  * Capacity: 100
  * Type: Machine Gun

## John Smith, The American
* Cyber Level: 50
* Demon Level: 0
* Athleticism: 3
* Base Speed: 0.85
* Field Proficiency: Combat Software. Chance to critical hit, dealing 250% damage
* Specialty: Pistol
* Unique: None
### - Weapons -
* SW1911
  * Slot: None
  * Ammo Type: Pistol
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 10
  * Type: Sidearm
* Vector
  * Slot: 2
  * Ammo Type: SMG
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 33
  * Type: SMG
  * Unique: Counteracts recoil
* Mossberg 590
  * Slot: 3
  * Ammo Type: Shotgun Shell
  * Damage Type: SmallArms
  * Range: 40m
  * Capacity: 7
  * Type: Shotgun
  * Alt Function: Pump action
* M4A1 with M203
  * Slot: 4
  * Ammo Type: AR/40mm
  * Damage Type: LongArms
  * Range: 500m
  * Capacity: 30/1
  * Type: Assault Rifle
  * Alt Function: Toggles between controlling the gun or the M203
  * Unique: Under barrel M203 Grenade Launcher
* FD388
  * Slot: 4 (Low priority)
  * Ammo Type: Sniper
  * Damage Type: LongArms 
  * Range: 1500m
  * Capacity: 10
  * Type: Sniper Rifle
  * Alt Function: Toggles 5x scope
* China Lake
  * Slot: 5
  * Ammo Type: 40mm
  * Capacity: 3
  * Type: Launcher 
  * Alt Function: Pump action
* M60E4
  * Slot: 6
  * Ammo Type: LMG
  * Damage Type: LongArms
  * Range: 1100m
  * Capacity: 100
  * Type: Machine Gun

## Wilhelm Stroheim, The German
* Cyber Level: 96
* Demon Level: 0
* Athleticism: 1
* Base Speed: 1.0
* Field Proficiency: Arm Brace. Removes recoil.
* Specialty: Machine Gun
* Unique: Resists physical damage; Weak to Electric
* Unique: Built in NVGs
### - Weapons -
* MK23
  * Slot: None
  * Ammo Type: Pistol
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 12
  * Type: Sidearm 
* UMP45
  * Slot: 2 
  * Ammo Type: SMG
  * Damage Type: SmallArms
  * Range: 65m
  * Capacity: 25
  * Type: SMG
* CAWS
  * Slot: 3 
  * Ammo Type: CAWS
  * Damage Type: LongArms
  * Range: 150m
  * Capacity: 10
  * Type: Shotgun
  * Alt Function: CAWS Magazine Loader. Loads 10 Shotgun Shells into a CAWS Magazine. Can be held to loop.
* G36KA4
  * Slot: 4 
  * Ammo Type: AR
  * Damage Type: LongArms
  * Range: 600m
  * Capacity: 30
  * Type: Assault Rifle
* WA2000
  * Slot: 4 (Low priority) 
  * Ammo Type: Sniper
  * Damage Type: LongArms
  * Range: 700m
  * Capacity: 10
  * Type: Sniper Rifle 
  * Alt Function: Toggles 5x scope
* M320
  * Slot: 5 
  * Ammo Type: PIKE
  * Capacity: 1
  * Type: Launcher
* MG4
  * Slot: 6 
  * Ammo Type: LMG
  * Damage Type: Long Arms
  * Range: 1000m
  * Capacity: 100
  * Type: Machine Gun

## Ofek Avadaan, The Israeli
* Cyber Level: 0
* Demon Level: 50
* Athleticism: 2
* Base Speed: 0.75
* Field Proficiency: Hellfire Rounds. Deals 25% additional Hellfire damage and shots explode
* Specialty: Shotgun
* Unique: Enchants explosives with his Hellfire, changing their properties 
  * Frag Grenade: Explodes on contact. Less shrapnel.
  * Claymore: No pellets. Larger explosive damage and radius. Lights ground on fire.
  * C4: Sprays fireballs on detonation 
### - Weapons -
* Desert Eagle
  * Slot: None 
  * Ammo Type: Pistol
  * Damage Type: SmallArms
  * Range: 200m
  * Capacity: 7
  * Type: Sidearm 
* Uzi
  * Slot: 2 
  * Ammo Type: SMG
  * Damage Type: SmallArms
  * Range: 200m
  * Capacity: 32
  * Type: SMG
* Remmington 870
  * Slot: 3 
  * Ammo Type: Shotgun Shell
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 7
  * Type: Shotgun 
  * Alt Function: Pump action 
* ACE-23
  * Slot: 4 
  * Ammo Type: AR
  * Damage Type: LongArms
  * Range: 500m
  * Capacity: 35
  * Type: Assault Rifle 
* Tavor 7
  * Slot: 4 (Low Priority) 
  * Ammo Type: Sniper
  * Damage Type: LongArms
  * Range: 800m
  * Capacity: 20
  * Type: Sniper Rifle
* Fireball 
  * Slot: 5 
  * Ammo Type: Mana
  * Damage Type: Fire
  * Type: Launcher
  * Unique: Not a weapon. Ofek casts a spell to fling a high explosive fireball. Casting can be Pistol Canceled.
  * Unique: Cannot be dropped. Ammo cannot be dropped.
* Negev NG-7
  * Slot: 6 
  * Ammo Type: LMG
  * Damage Type: LongArms
  * Range: 700m
  * Capacity: 100
  * Type: Machine Gun

## Father Ward, The Italian
* Cyber Level: 0
* Demon Level: 0
* Athleticism: 0
* Base Speed: 0.55
* Field Proficiency: Blessed Ammo. Deals 25% additional Holy damage
* Specialty: None
* Unique: No melee, has a warding cross instead
### - Weapons -
* Cross 
  * Slot 1
  * Main Fire: Punch with fists
  * Alt Fire: Deals constant low Holy damage in a short range with the cross. Pushes enemies back
  * Reload: Cleanse Corruption with the cross and a prayer.
  * Unique: Loses effectiveness as Demon Level rises. Actions that use the cross have a invisible limited resource that recharges over time. 
* 92FS
  * Slot: None 
  * Ammo Type: Pistol
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 15
  * Type: Sidearm 
* PMX
  * Slot: 2 
  * Ammo Type: SMG
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 30
  * Type: SMG
* M4 
  * Slot: 3 
  * Ammo Type: Shotgun Shell
  * Damage Type: SmallArms
  * Range: 50m
  * Capacity: 7
  * Type: Shotgun
* ARX-160
  * Slot: 4 
  * Ammo Type: AR
  * Damage Type: LongArms
  * Range: 600m
  * Capacity: 30
  * Type: Assault Rifle 
* ARX-200
  * Slot: 4 (Low Priority) 
  * Ammo Type: Sniper
  * Damage Type: LongArms
  * Range: 1000m
  * Capacity: 20
  * Type: Sniper Rifle
  * Alt Function: Toggles 4x scope
* Bible
  * Slot: 5 
  * Ammo Type: Exorcism Cross
  * Damage Type: Holy
  * Type: Launcher
  * Unique: Recite the Pslams to smite all enemies in a large area in front. Recital can be Pistol Canceled.
* Minimi
  * Slot: 6 
  * Ammo Type: LMG
  * Damage Type: LongArms
  * Range: 700m
  * Capacity: 100
  * Type: Machine Gun