class FOTGBaseCasing : Actor 
{
	int 	Smoker;
	double RollAmount;
	property BaseSpin : RollAmount;
	
	override void BeginPlay()
	{
		RollAmount = RollAmount * frandom(0.75, 1.75);
		Smoker	= random(105, 350);
		
		super.BeginPlay();
	}
	
	override void Tick()
	{
		if(RollAmount > 6)
		{
			RollAmount -= 0.5;
		}
		
		if(Smoker > 0)
		{
			Smoker --;
		}
		
		if(Smoker && FOTG_Smoke) A_SpawnItemEX("FOTGBulletSmoke", zvel: frandom(0, 1));
		
		A_ScaleVelocity(0.98);
	
		super.Tick();
	}

	Default 
	{
		Projectile;
		
		Height 	4;
		Radius 	2;
		Mass 	1;
		
		Scale	0.15;
		
		BounceType "Doom";
		BounceFactor 0.65;
		WallBounceFactor 0.20;
		
		-NOGRAVITY;
		+MOVEWITHSECTOR;
		+ROLLSPRITE;
		+ROLLCENTER;
		+FORCEXYBILLBOARD;
		+USEBOUNCESTATE;
		+THRUACTORS;
	}
	
	States 
	{
		Spawn:
			#### B 1 NoDelay;
			Goto FlyA;
			
		FlyA: 
			#### B 0 A_JumpIf(Roll < -180, "FlyB");
			#### B 1 A_SetRoll(Roll - self.RollAmount);
			Loop;
			
		FlyB:
			#### A 0 A_JumpIf(Roll < -360, "Spawn");
			#### A 1 A_SetRoll(Roll - self.RollAmount);
			Loop;
			
		Bounce:
			#### # 0 
			{
				A_SetRoll(Roll - self.RollAmount);
				self.RollAmount = self.RollAmount * 1.05;
			}
			Goto FlyA;
			
		Death:
			#### # 0 
			{ 
				if(Roll < -180) bSpriteFlip = TRUE; 
				
				if(!GetFloorTerrain().IsLiquid && (pos.z <= floorz + 4)) A_SetRoll(0);
			}
			#### # 0 A_Jump(256, "Death1", "Death2");
		Death1:
			#### C -1
			{
				int LifeTime = 35 * GetCVar("FOTG_CasingLife");
				
				if(LifeTime > 0) A_SetTics(LifeTime);
			}
			Goto Faded;
			
		Death2:
			#### D -1
			{
				int LifeTime = 35 * GetCVar("FOTG_CasingLife");
				
				if(LifeTime > 0) A_SetTics(LifeTime);
			}
			Goto Faded;
			
		Faded:
			#### ########## 1 A_FadeOut(0.1);
			Stop;
	}
}

class FOTGCasingSpawnerBase : Actor 
{
	string CasingToSpawn;
	int		YSpeed;
	int		ZSpeed;
	
	property Casing : CasingToSpawn;
	property Sped: YSpeed;
	property Zped: ZSpeed;

	Default 
	{
		Projectile;
		
		Height 3;
		Radius 2;
		Speed 12;
		
		+NOINTERACTION;
	}
	
	States 
	{
		Spawn:
			TNT1 A 1 NoDelay;
			TNT1 A 1 
			{
				if(FOTG_casings) A_SpawnItemEx(self.CasingToSpawn, xvel: self.YSpeed * frandom(-0.15, 0.15), yvel: self.YSpeed * frandom(0.5, 1.5), zvel: ZSpeed * frandom(-1.5, 1.5));
			}
			Stop;
	}
}

class FOTGPistolCasing : FOTGBaseCasing
{
	Default 
	{
		Scale 0.20;
	
		BounceSound	"CasingBounce";
		DeathSound	"CasingBounce";
	
		FOTGBaseCasing.BaseSpin 36;
	}

	States 
	{
		Spawn:
			X71C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTGPistolCasingSpawner : FOTGCasingSpawnerBase
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTGPistolCasing";
		FOTGCasingSpawnerBase.Sped 12;
		FOTGCasingSpawnerBase.Zped 2;
	}
}

class FOTGSMGCasingSpawner : FOTGPistolCasingSpawner
{
	Default 
	{
		FOTGCasingSpawnerBase.Sped 14;
		FOTGCasingSpawnerBase.Zped 2;
	}
}

class FOTGShotgunCasing : FOTGBaseCasing
{
	Default 
	{
		Scale	0.08;
	
		BounceSound	"ShellBounce";
		DeathSound	"ShellBounceEnd";
	
		FOTGBaseCasing.BaseSpin 42;
	}

	States 
	{
		Spawn:
			X69C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTGShotgunCasingSpawner: FOTGCasingSpawnerBase
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTGShotgunCasing";
		FOTGCasingSpawnerBase.Sped 10;
		FOTGCasingSpawnerBase.Zped 0;
	}
}

class FOTGRifleCasing : FOTGBaseCasing
{
	Default 
	{
		BounceSound	"CasingBounce";
		DeathSound	"CasingBounce";
		
		FOTGBaseCasing.BaseSpin 30;
	}
	
	States 
	{
		Spawn:
			X70C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTGRifleCasingSpawner : FOTGCasingSpawnerBase
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTGRifleCasing";
		FOTGCasingSpawnerBase.Sped 24;
		FOTGCasingSpawnerBase.Zped 2;
	}
}

class FOTGSniperCasing : FOTGBaseCasing
{
	Default 
	{
		BounceSound	"CasingBounce";
		DeathSound	"CasingBounce";
		
		FOTGBaseCasing.BaseSpin 24;
	}
	
	States 
	{
		Spawn:
			X74C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTGSniperCasingSpawner : FOTGCasingSpawnerBase
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTGSniperCasing";
		FOTGCasingSpawnerBase.Sped 24;
		FOTGCasingSpawnerBase.Zped 2;
	}
}

class FOTGBigSniperCasing : FOTGSniperCasing
{
	States 
	{
		Spawn:
			X73C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTGBigSniperCasingSpawner : FOTGSniperCasingSpawner
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTGBigSniperCasing";
	}
}

class FOTG40mmCasing : FOTGBaseCasing
{
	Default 
	{
		Mass 	11;
		Scale 	0.10;
		
		BounceSound	"ShellBounce";
		DeathSound	"ShellBounceEnd";
	
		FOTGBaseCasing.BaseSpin 18;
		
		+EXPLODEONWATER;
		+FLOORCLIP;
	}
	
	States 
	{
		Spawn:
			X72C B 1 NoDelay A_SetRoll(0);
			Goto FlyA;
	}
}

class FOTG40MMCasingSpawner : FOTGCasingSpawnerBase
{
	Default 
	{
		FOTGCasingSpawnerBase.Casing "FOTG40mmCasing";
		FOTGCasingSpawnerBase.Sped 8;
		FOTGCasingSpawnerBase.Zped 0;
	}
}