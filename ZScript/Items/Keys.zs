class FOTGKey : DoomKey 
{
	string GetMSG;

	override void AttachToOwner(Actor user)
	{
		let plr = BaseCharacter(user);
	
		if(plr)
		{
			plr.A_Print(self.GetMSG);
		}
	
		Super.AttachToOwner(user);
	}

	Default 
	{
		Inventory.PickupMessage "";
	}
}

class FOTGSkull : DoomKey 
{
	string GetMSG;

	override void AttachToOwner(Actor user)
	{
		let plr = BaseCharacter(user);
	
		if(plr)
		{
			plr.A_Print(self.GetMSG);
		}
	
		Super.AttachToOwner(user);
	}

	Default
	{
		Scale 0.45;
	
		FloatBobStrength 0.25;
		FloatBobPhase	 10;	
		
		Inventory.PickupMessage "";
		Inventory.PickupSound	"SPOOK";
		
		+FLOATBOB;
	}
}

class FOTGRedKey : FOTGKey replaces RedCard 
{
	Default 
	{	
		Species "RedCard";
	}	
	
	States { Spawn: RKII AB 8 NoDelay { self.GetMSG = "Picked up a \crred keycard"; } Loop; }
}

class FOTGBlueKey : FOTGKey replaces BlueCard 
{
	Default 
	{
		Species "BlueCard";
	}
	
	States { Spawn: BKII AB 8 NoDelay { self.GetMSG = "Picked up a \cnblue keycard"; } Loop; }
}

class FOTGYellowKey : FOTGKey replaces YellowCard 
{
	Default 
	{
		Species "YellowCard";
	}
	
	States { Spawn: YKII AB 8 NoDelay { self.GetMSG = "Picked up a \cfyellow keycard"; } Loop; }
}

class FOTGRedSkull : FOTGSkull replaces RedSkull
{
	Default
	{
		Species "RedSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULR ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "Picked up a \caruby skull"; } 
			Loop; 
	}
}

class FOTGBlueSkull : FOTGSkull replaces BlueSkull
{
	Default
	{
		Species "BlueSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULB ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "Picked up a \cysapphire skull"; } 
			Loop; 
	}
}

class FOTGYellowSkull : FOTGSkull replaces YellowSkull
{
	Default
	{
		Species "YellowSkull";
	}
	
	States 
	{ 
		Spawn: 
			KULY ABCDEFGHIJKLMNOP 3 NoDelay { self.GetMSG = "Picked up a \cxtopaz skull"; } 
			Loop; 
	}
}