Welcome to Feet On The Ground. 
This readme is designed as a quick start guide. For more detailed information, see the included manual.

This mod requires a new approach to a lot of situations Doom throws at you, and some maps may be impossible to complete. 

If this is an issue, turn back now.

...

If your still with me, here are a few concepts to always keep in mind.

- You have to press Use to manually pick things up
- You can mantle by holding jump
- You have a limited amount of stamina, and running out sucks balls. Don't!
- There is a weight system. It effects your your movement speed, stamina drain and recharge rates.
- You can store items in a rucksack. Slot 1 under melee.
- You can customize your ammo capacities with a MOLLE manager. Slot 1 under the rucksack.
- You can slide by crouching while running
- Your move speed is generally lower than vanilla
- Nothing is used on pickup and has to be manually used. More on this later.
- Armor is very effective but not very durable. With your low HP total, it is required to survive.

--- WEAPONS ---
- All guns have to be maintained and can break. 
- Keeping your guns clean keeps the chance of jams and misfires to a minimum (but these never go away completely!)
- If a gun does jam, Reload attempts to Unjam it with a high failure rate. Maintainenance unjams the gun 100%.
- Guns get dirty from shooting, reloading, dropping them and very slowly over time.
- Different types of guns have different base jam chances but all have the same amount of Max Condition. 
- Different parts of the gun need to be maintained and have their own damage tracking. They can only be maintained when above 35% and below 85%.
- Parts below 35% are considered Broken and have to be replaced.
- There is a threshold to how clean your guns can be based on the total of the 4 Gun Parts health.
- Repair kits can spawn over Chainsaws and rarely with backpacks. They are used automatically while maintaining to repair broken parts.
- On weapon slot 1, under melee, is the Ammo Manager. Can be used to drop all of one type of ammo, and manage ammo for weapons you have not picked up yet.
- There are advantages to keeping the same copy of a gun with you.
- When you have them, press 'F' to toggle NVGs

--- BODILY DAMAGE ---
When taking damage, your body parts will take damage randomly. Various penalties are applied at low Body Part health. Body Parts are healed a base flat amount by Medikits, as well as as your HP heals.
- Head: Pain response is worse. Vision is obsucured when wounded. Severe is obscurced more.
- Body: Max Stamina is reduced. Bleed out when Wounded. Severe is faster bleed out and Stamina bleed.
- Arms: Recoil is increased. Wounded multiplies Recoil penalty.
- Legs: Speed and Carry weight is reduced. Wounded increases Speed penalty. Severe increases it further.


- GUN CONTROLS -
You can hold RELOAD and ALT FIRE to bring up a controls tooltip
Main Fire  - Shoot
Alt Fire   - Function. Usually works the pump, scopes, or fires both barrels.
Reload     - Reloads. If your gun is jammed, attempts to unjam it.
Zoom       - Drops your gun to quick draw your pistol. Reloading and weapon maintanence can be interrupted with this.  
User 1     - Throws a grenade. Hold it to throw a stun grenade.
User 2     - Maintains the gun. Mainfire can skip ahead in maintainenance, Alt Fire can end it prematurely and holding Reload uses a repair kit early.
User 3     - Unload the gun. Adds the ammo to your inventory and drops the gun. If you don't have a full magazine, just drops the gun.
User 4     - Drops ammo for the gun from your inventory. For weight management and sharing purposes

- MELEE CONTROLS -
Fighting with melee drains stamina. 
Main Fire  - Punch
Zoom       - Draws your pistol
User 1     - Throw a Grenade

- RUCKSACK CONTROLS -
Main Fire   - Retrieve selected item 
Alt Fire    - Store selected item 
Reload      - Cycle to next item 
Zoom        - Cycle to previous item
User 1      - Cycle to next pocket 
User 2      - Cycle to previous pocket
User 3      - Drop selected item. If rucksack is empty, drops item from inventory instead
User 4      - Dump all items from the rucksack 

- MOLLE CONTROLS - 
Main FIRE	- Drop selected pouch
Reload		- Cycle to next pouch 
Zoom 		- Cycle to previous pouch

--- ITEMS ---
Items are not used automatically on pickup, nor are they used with ZDoom's inventory system.
Instead, on slot 8, 9 and 0 you have Tactical Gear, Medical Gear and Magical Gear respectively. 

- MEDICAL CONTROLS -
Main Fire   - Use a medikit. Medikits take a few seconds to use, and heal you slowly over time, but heal a great amount. The lower your health the more they heal. Heals Bodily Damage.
Alt Fire    - Use a stimpack. Stimpacks very quickly heal you a small amount, restore a chunk of your stamina, and grants a 90 second buff that increases strength and stamina regen.
Reload      - Drink a potion. Drinks a blue potion dose that overheals by 1 HP. Overhealing increases strength.
Zoom        - Draws your sidearm
User 1      - Use an armor plate. Applies a plate to your armor that restores its amount by 1. Can 'overheal' armor. Does nothing with no armor on
User 2      - Use a berserk. Berserks quickly heal you a great amount, fully restore your stamina and greatly increase your melee strength. They reduce your general stablity while in effect however.
User 3      - Use a hazmat suit.
User 4      - Toggles Drop Mode. When Drop Mode is ON, instead of using the items with the above buttons, you drop them. For weight management and sharing purposes.

- TACTICAL GEAR CONTROLS -
Main Fire   - Plant a Claymore. Arms after a second. Explodes and sprays 300-700 steel balls in a wide area when an enemy walks in front of it.
Alt Fire    - Throw a C4. C4 arms as soon as it lands. Can stick to walls. Won't explode until detonated. Shooting it does not cause it to explode. Can destroy walls and doors.
Reload      - Detonate all planted C4
Zoom        - Draws your sidearm
User 1      - Throws a grenade
User 2      - Throws a stun grenade. Does very little damage and freezes enemies hit for several seconds.
User 3      - N/A
User 4      - Toggles Drop Mode. When Drop Mode is ON, instead of using the items with the above buttons (sans the pistol), you drop them. For weight management and sharing purposes.

- MAGICAL GEAR CONTROLS -
Main Fire   - Use a Invulnerablity Sphere, granting unlimited Stamina, immunity to damage and knockback 
Alt Fire    - Use a Blur Ring, granting Blur and protection 
Reload      - Use a Megasphere, overhealing a large amount and granting magic armor
Zoom        - Draws your sidearm
User 1      - Use a computer map 
User 2      - Drop your NV goggles. For weight management and sharing purposes 
User 3      - Drop a repair kit. For weight management and sharing purposes
User 4      - Toggles Drop Mode. When Drop Mode is ON, instead of using the items with the above buttons, you drop them. For weight management and sharing purposes.


--- CHARACTERS ---
There will be four characters to choose from, each with their own arsenals, strengths and weaknesses. 
Their weapons are based on thier country of origin

- 'HELGA' -
Name: Nadia [CLASSIFIED]
Origin: Russia
Other Aliases: Valkyrie, Queen Kong, Hel
Age: [REDACTED]
Sex: Female
Height: 5' 9" 
Weight: 155 lbs
Strengths: Fully effected by magic, strong overall arsenal with access to the strongest weapon
Weaknesses: Human with low stats
Bio: Well trained and decorated former Spetznaz agent. Performed extremely well on performance tests, and well trained in sniping, demolitions and espionage. Credited with the sucessfully sniping assassination of [REDACTED] of [REDACTED]. Confident, focused and cold.

- JOHN SMITH -
Name: [DATA EXPUNGED]
Origin: America
Other Aliases: Grim Reaper, Million Dollar Man
Age: [DATA EXPUNGED]
Sex: Male
Height: 5' 4"
Weight: 365 lbs
Strengths: Very well rounded stats and arsenal
Weaknesses: Lower effect from magic, no very strong skills
Bio: Secret Black Ops agent who doesn't offically exsist. Part of an experimental line of cyborgs who are able to fully blend in with humans. Exact activites are unknown, but confirmed to be a major part of OPERATION [REDACTED] and several other secret wars. Reserved and serious, rarely engaging in any kind of personal conversation.

- WILHELM STROHEIM -
Name: Wilhelm Stroheim
Origin: Germany
Other Aliases: Cyborg, Tin Man, Terminator, other famous movie robots
Age: 24
Sex: N/A; formally Male
Height: 6' 10"
Weight: 935 lbs
Unique: Has built in NVGs
Unique: Resists most physical attacks, but is weak to electric and plasma
Strengths: Very high stats, fastest movement, improved effect from healing and repairing
Weakness: Greatly reduced effect from magic, long trigger delay and reloading times from lack of fine motor skills
Bio: Heavy combat cyborg. Cybernized after sustaining mortal injury on the front line in the war against the demonic invasion. Body was recovered for Project Ubersoldat and made into a killing machine. Nearly entirely machine, with only his heart and parts of his central nervous system remaining human. Surprising upbeat and optimistic for a dead man who is 96% machine.

- OFEK AVADAAN -
Name: Ofek Avadaan
Origin: Israel
Other Aliases: Dan, Devilman, Cerberus
Age: 89
Sex: Male
Height: 6' 3" 
Weight: 333 lbs
Unique: Imbues explosive with his magic, changing their properties
Strengths: Greater effect from magic, maintains full control with berserk
Weakness: Reduced effect from normal healing, demons are always alerted of his presence
Bio: Half demon agent of the secret society [REDACTED] who have been battling demons and witchcraft since the time of the Crusades. Imbued with demon's blood 60 years ago and has grown to be quite skilled in its use. Mutated into a half man and half pinky hybrid. Gloomy with a nasty temper, loves to make others uncomfortable.

- FATHER WARD -
Name: John Ward
Origin: America (operates from the Vatican)
Other Aliases: None
Age: 46
Sex: Male
Height: 5' 10"
Weight: 140 lbs
Unique: No melee, but has a cross to damage and push back nearby enemies.
Unique: Has The Lord's divine protection, resisting certain damage types
Strengths: Access to holy damage, which all demons are weak to. Powerful arsenal.
Weakness: Human with the lowest stats. Corruption and demon levels weaken Holy effects.
Bio: A priest from the Vatican. He has no offical rank and is operating as a independant contractor for The Team. Veteran demon slayer and exrocist. Quiet, but pious. Often prays to himself.